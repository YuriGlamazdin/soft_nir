#include "TimerOne.h"
#define SECONDS_TO_RESET 30

int reset_count = 0;
char inByte[128];
int i = 0;
void setup() {
  Serial.begin(9600);
  pinMode (13, OUTPUT);
  Timer1.initialize(1000000);
  Timer1.attachInterrupt(res_on_timer);
}
void res_on_timer() {
  
  if (reset_count >= SECONDS_TO_RESET) {
    reset_count = 0;
    cli();
  digitalWrite(13, HIGH);
  delay(200000);
  digitalWrite(13, LOW);
  sei();
  }
  else {
    reset_count++;
  }
}
void reset() {
cli();
  digitalWrite(13, HIGH);
  delay(200000);
  digitalWrite(13, LOW);
  reset_count = 0;
sei();
}
void loop() {

  while (Serial.available()) {
    char newByte = Serial.read();
    //Serial.println(newByte);

    if (newByte == '\n') {
      //Serial.println(inByte);
      if (strcmp("Ok", inByte) == 0) {
        //Serial.println("if");
        reset_count = 0;
      }
      else {
        reset();
      }
      for (int j = 0; j <= i; j++) {
        inByte[j] = 0;
      }
      i = 0;
    }

    else {
      inByte[i] = newByte;
      i++;
    }
  }
}

