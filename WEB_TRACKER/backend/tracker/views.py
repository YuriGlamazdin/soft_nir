# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import auth
from django.shortcuts import render

from tracker import models
from django.http import JsonResponse

# Create your views here.


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def tracker(request):
    position = models.Position.objects.all()
    context = {
        "position": position,
        "client_ip" : get_client_ip(request)
    }

    return render(request, 'tracker.html', context)


def save_page(request):
    loc_count = models.Position.objects.count()

    context = {
        "count": loc_count,
    }
    return render(request, 'save.html', context)


def delete_page(request):
    loc_count = models.Position.objects.count()

    context = {
        "count": loc_count,
    }
    return render(request, 'delete.html', context)


def delete_all_position(request):
    models.Position.objects.all().delete()
    response = {
        'status': 'ok'
    }
    return JsonResponse(response)


def handle_uploaded_file(f):
    with open('C:\\Users\\zinjk\\Desktop\\dd\\soft_nir\\WEB_TRACKER\\backend\\media\\image\\data2.png', 'wb+') as dest:
        for chunk in f.chunks():
            dest.write(chunk)


def save_new_position(request):
    longitude = 0
    latitude = 0
    if request.method == 'GET':
        longitude = request.GET['longitude']
        latitude = request.GET['latitude']
    if request.method == 'POST':
        longitude = request.POST['longitude']
        latitude = request.POST['latitude']
        image = request.FILES['image']
    handle_uploaded_file(image)
    pos = models.Position()
    pos.longitude_gps = longitude
    pos.latitude_gps = latitude
    pos.save()
    response = {
        'status': 'ok'
    }
    return JsonResponse(response)


def vue_tracker(request):
    return render(request, 'index.html')


def command(request):
    if request.method == "POST":
        if int(request.POST['choose']) == 0:
            command = models.Command()
            command.longitude = request.POST['longitude']
            command.latitude = request.POST['latitude']
            command.save()
        elif int(request.POST['choose']) == 1:
            models.Command.objects.all().update(sended=False)
        elif int(request.POST['choose']) == 2:
            models.Command.objects.all().delete()
    points = models.Command.objects.values('latitude', 'longitude', 'sended').all()
    context = {
        "points": points
    }
    return render(request, 'command.html', context)
