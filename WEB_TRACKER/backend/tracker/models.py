# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from django.db import models
from django import forms
# Create your models here.


def get_image_path(instance, filename):
    return os.path.join('photo/', filename)


class Position(models.Model):
    image = models.ImageField(blank=True, upload_to=get_image_path, null=True)
    longitude_gps = models.FloatField()
    latitude_gps = models.FloatField()
    vcc = models.FloatField(blank=True, null=True)
    yaw_compass = models.FloatField(blank=True, null=True)
    altitude_gps = models.FloatField(blank=True, null=True)
    yaw_gps = models.FloatField(blank=True, null=True)
    speed_gps = models.FloatField(blank=True, null=True)
    satellites_gps = models.FloatField(blank=True, null=True)
    hdop_gps = models.FloatField(blank=True, null=True)
    latitude_point = models.FloatField(blank=True, null=True)
    longitude_point = models.FloatField(blank=True, null=True)
    ip_client = models.CharField(blank=True, null=True, max_length=255)
    created_at = models.DateTimeField(auto_now=True, null=True)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = "Новость"
        verbose_name_plural = "Новости"


class Command(models.Model):
    longitude = models.FloatField()
    latitude = models.FloatField()
    sended = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now=True, null=True)

    def __unicode__(self):
        return self.created_at

    class Meta:
        verbose_name = "Command"
        verbose_name_plural = "Commands"


class PositionForm(forms.Form):
    photo = models.ImageField(blank=True, upload_to=get_image_path)
    longitude = models.FloatField()
    latitude = models.FloatField()

