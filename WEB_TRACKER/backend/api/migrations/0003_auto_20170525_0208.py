# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-05-24 16:08
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_boardcommand'),
    ]

    operations = [
        migrations.CreateModel(
            name='Path',
            fields=[
                ('positions', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='api.BoardInfo')),
                ('created_at', models.DateTimeField(auto_now=True, null=True)),
            ],
            options={
                'verbose_name': 'Board command',
                'verbose_name_plural': 'Board command',
            },
        ),
        migrations.AlterModelOptions(
            name='boardcommand',
            options={'verbose_name': 'Board command', 'verbose_name_plural': 'Board command'},
        ),
    ]
