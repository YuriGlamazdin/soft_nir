from __future__ import unicode_literals

from django.db import models

# Create your models here.


class BoardImage(models.Model):
    image = models.ImageField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now=True, null=True)

    def __unicode__(self):
        return self.created_at

    class Meta:
        verbose_name = "Image"
        verbose_name_plural = "Images"


class BoardGPSPosition(models.Model):
    longitude_gps = models.FloatField()
    latitude_gps = models.FloatField()
    altitude_gps = models.FloatField(blank=True, null=True)
    yaw_gps = models.FloatField(blank=True, null=True)
    hdop_gps = models.FloatField(blank=True, null=True)
    speed_gps = models.FloatField(blank=True, null=True)
    satellites_gps = models.FloatField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now=True, null=True)

    def __unicode__(self):
        return self.created_at

    class Meta:
        verbose_name = "GPS Postition"
        verbose_name_plural = "GPS Positions"


class BoardInfo(models.Model):
    board_image = models.OneToOneField(
        BoardImage,
        on_delete=models.CASCADE,
        null=True
    )
    gps_position = models.OneToOneField(
        BoardGPSPosition,
        on_delete=models.CASCADE,
        primary_key=True
    )
    vcc = models.FloatField(blank=True, null=True)
    yaw_compass = models.FloatField(blank=True, null=True)
    latitude_point = models.FloatField(blank=True, null=True)
    longitude_point = models.FloatField(blank=True, null=True)
    ip_client = models.CharField(blank=True, null=True, max_length=255)
    created_at = models.DateTimeField(auto_now=True, null=True)

    def __unicode__(self):
        return self.created_at

    class Meta:
        verbose_name = "Board state"
        verbose_name_plural = "Board state"


class BoardCommand(models.Model):
    gps_position = models.OneToOneField(
        BoardGPSPosition,
        on_delete=models.CASCADE,
        primary_key=True
    )
    created_at = models.DateTimeField(auto_now=True, null=True)

    def __unicode__(self):
        return self.created_at

    class Meta:
        verbose_name = "Board command"
        verbose_name_plural = "Board command"


