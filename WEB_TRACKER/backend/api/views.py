from django.core import serializers
from django.shortcuts import render

from tracker import models
from api import models as apiModel
from django.http import JsonResponse
import json

# Create your views here.

def validate_key(key):
    return key == "around"


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def position_post(request):
    payload = json.loads(request.body)
    print payload
    key = payload['key']
    payload = payload['data']
    if validate_key(key):
        payload['ip_client'] = get_client_ip(request)
        models.Position(**payload).save()
        respounse = {
            "status": True
        }
    else:
        respounse = {
            "status": False
        }
    return JsonResponse(respounse)


def position_get(request):
    data = serializers.serialize('json', models.Position.objects.all())
    respounse = {
        "data": data
    }
    return JsonResponse(respounse)


def position_gets(request):
    i = int(request.GET['item'])
    obj = models.Position.objects.all()[i:i+1]
    data = serializers.serialize('json', obj)
    respounse = {
        "data": data
    }
    # print data
    return JsonResponse(respounse)


def get_position_count(request):
    respounse = {
        "data": models.Position.objects.count()
    }

    return JsonResponse(respounse)


def get_command_board(request):
    data = models.Command.objects.select_for_update().filter(sended=False)

    send_data = data.values('longitude', 'latitude').all()
    respounse = {
        "data": list(send_data)
    }
    data.update(sended=True)
    return JsonResponse(respounse)
