"""AroundIsland URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

import tracker.views as trackerViews
import api.views as apiViews

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', trackerViews.tracker, name="tracker"),
    url(r'^save/$', trackerViews.save_new_position, name="save"),
    url(r'^save_page/$', trackerViews.save_page, name="save_page"),
    url(r'^delete_all/$', trackerViews.delete_all_position, name="delete_all_location"),
    url(r'^delete/$', trackerViews.delete_page, name="delete_page"),
    url(r'^vue/$', trackerViews.vue_tracker, name="vue"),
    url(r'^command/$', trackerViews.command, name="command"),


    url(r'^api/position/post/$', apiViews.position_post, name="position_post"),
    url(r'^api/position/get/$', apiViews.position_get, name="position_get"),
    url(r'^api/position/gets/$', apiViews.position_gets, name="position_gets"),
    url(r'^api/position/count/$', apiViews.get_position_count, name="get_position_count"),
    url(r'^api/command/get/$', apiViews.get_command_board, name="get_command_board"),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

