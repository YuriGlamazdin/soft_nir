# coding=utf-8
import urllib2, json


def post_position_test():
    url = 'http://127.0.0.1:8000/api/position/post/'

    im = open("data2.png", "rb")
    binary = im.read().encode("base64")


    postdata = {
        "data": {
            # обязательные данные
            # 3.130292, 131.889492 владивосток
            "latitude_gps": "43.130292",  # //широта
            "longitude_gps": "131.889492 ",  # //долгота
            # #  не обязательные данные
            # "vcc": "1",  #  - напряжение на борту
            # "yaw_compass": "1",  # курс по компасу
            # "altitude_gps": "1",  # //-высота
            # "yaw_gps": "1",  # - курс
            # "speed_gps": "1",  # - скорость
            # "satellites_gps": "1",  # - количество обнаруженных спутников
            # "hdop_gps": "1",  #  - размытие точности
            # "latitude_point": "1",  # координаты текущей цели, куда движется лодка
            # "longitude_point": "1",
            "image": binary
        },
        "key": "around"  # ключ безопасноти
    }
    # print postdata
    req = urllib2.Request(url)
    req.add_header('Content-Type', 'application/json')
    data = json.dumps(postdata)

    response = urllib2.urlopen(req, data)
    print response.read()


def position_get_test():
    url = 'http://95.154.96.240:8000/api/position/get/'
    req = urllib2.Request(url)
    #req.add_header('Content-Type', 'application/json')

    response = urllib2.urlopen(req)
    print response.read()


# position_get_test()
post_position_test()

