import time
import serial
import urllib
import urllib2, json
import cv2
import datetime
import os
import logging

logging.basicConfig(format = u'%(levelname)-8s [%(asctime)s] %(message)s', level = logging.DEBUG, filename = u'/home/pi/nir/log.log')

logging.info( u'Start' )

PORT_NAME = "/dev/ttyACM0"
CAMERA_NAME = 0  # for /dev/video0

# longitude_gps = 131.889492
# latitude_gps=43.126893

longitude_gps = 0.0
latitude_gps = 0.0
vcc = 0
yaw_compass = 0
altitude_gps = 0
yaw_gps = 0
speed_gps = 0
satellites_gps = 0
hdop_gps = 0
latitude_point = 0
longitude_point = 0

cap = cv2.VideoCapture(0)



def post_position_test():
    try:
        #str1 = "http://95.154.96.240:8000/save/?longitude=" + str(longitude_gps) + "&latitude=" + str(latitude_gps)
        str1 = "longitude=" + str(longitude_gps) + "  latitude=" + str(latitude_gps)
        # file.write(str1)
        print(str1 + " /r/n ")


        # response = urllib2.urlopen(str1)

        url = 'http://95.154.96.240:8000/api/position/post/'
        #url = 'http://85.95.150.144:666/api/position/post/'
        print (url)

        # cv2.imwrite('/home/pi/nir/2.jpg', image)
        # im = open("/home/pi/nir/2.jpg", "rb")
        filename = datetime.datetime.now().strftime("/home/pi/nir/img/%Y_%m_%d_%H_%M_%S.jpg")
        print(filename)

        r=1
        while r<100:
            r=r+1
            ret, image = cap.read()


        ret, image = cap.read()

        cv2.imwrite(str(filename), image)

        flag= False
        while flag==False:
            b = os.path.getsize(str(filename))
            print(b)
            if b>0:
                flag=True
        #time.sleep(2)

        im = open(str(filename), "rb")
        binary = im.read().encode("base64")




        postdata = {
            "data": {
                # 43.130292, 131.889492
                "latitude_gps": str(latitude_gps),
                "longitude_gps": str(longitude_gps),
                "vcc": str(vcc),  #
                "yaw_compass": str(yaw_compass),  #
                "altitude_gps": str(altitude_gps),  #
                "yaw_gps": str(yaw_gps),  # -
                "speed_gps": str(speed_gps),  # -
                "satellites_gps": str(satellites_gps),  # -
                "hdop_gps": str(hdop_gps),  # -
                "latitude_point": str(latitude_point),  #
                "longitude_point": str(longitude_point),
                "image": binary
            },
            "key": "around"  #
        }
        #print (postdata)

        req = urllib2.Request(url)
        req.add_header('Content-Type', 'application/json')
        data = json.dumps(postdata)

        # print (data)
        response = urllib2.urlopen(req, data)
        logging.info(u'response server ' + str(response))
        print(response.read())
    except Exception:
        logging.error(u'error send to server ' + str(Exception))


def position_get_test():
    url = 'http://95.154.96.240:8000/api/position/get/'
    req = urllib2.Request(url)
    # req.add_header('Content-Type', 'application/json')

    response = urllib2.urlopen(req)
    print(response.read())


def get_frame():
    success, image = cap.grab()
    # We are using Motion JPEG, but OpenCV defaults to capture raw images,
    # so we must encode it into JPEG in order to correctly display the
    # video stream.
    ret, jpeg = cv2.imencode('.jpg', image)
    return jpeg.tobytes()


if __name__ == "__main__":
    try:
        port = serial.Serial(PORT_NAME, 115200, timeout=5)
        port.flush()
    except Exception:
        logging.error(u'error connect arduino' + str(Exception))


    # port.readline()
    # port.baudrate = Robot.BAUD_RATE
    # port.write_timeout = Robot.WRITE_TIMEOUT
    # serPort = "/dev/ttyACM0"
    # baudRate = 115200
    # port = serial.Serial(serPort, baudRate)

    port.dtr = True
    time.sleep(2)  # some Arduino-related bugfix

    hostname = "95.154.96.240"  # example
    # hostname = "8.8.8.8"  # example



    print("seng home")
    # post_position_test()



    # post_position_test()
    # str_home = "http://95.154.96.240:8000/save/?longitude=131.91809&latitude=43.087212"
    # response = urllib2.urlopen(str_home)
    # print(response.info())
    #
    # print("seng taget")
    # str_home = "http://95.154.96.240:8000/save/?longitude=131.928481&latitude=43.078834"
    # response = urllib2.urlopen(str_home)
    # print(response.info())
    # time.sleep(10)




    # threading.Thread(target=robo.follow_line).start()
    while (True):
        ret, image = cap.read()
        response = os.system("ping -c 1 " + hostname)
        # and then check the response...
        if response == 0:
            port.write('inet1|')
            # print(hostname, 'is up!')
        else:
            port.write('inet0|')
            # print(hostname, 'is down!')

        time.sleep(1)

        port.write('GPS|')

        port.flushInput()
        port.flushOutput()
        out = ""
        time.sleep(2)
        while port.inWaiting() > 0:
            out += port.read(1)
        print(out)

        data = out.split('|')
        print(data)

        # str1 = "http://192.168.88.11:8000/save/?longitude=131&latitude=43"
        # str1 = "http://95.154.96.240:8000/save/?longitude=131&latitude=43"
        # print(str1)
        # ret = urllib2.urlopen(str1)


        s2 = 0
        s1 = 0

        if len(data) >= 2:
            if data[0] == 'GPSDATA':
                # print(data[1], data[2])
                latitude_gps = float(data[1]) / 1000000.0
                longitude_gps = float(data[2]) / 1000000.0
                satellites_gps = data[3]
                yaw_gps = data[4]
                speed_gps = data[5]
                altitude_gps = data[6]
                hdop_gps = data[7]
                yaw_compass = data[8]
                vcc = data[9]
                latitude_point = float(data[10]) / 1000000.0
                longitude_point = float(data[11]) / 1000000.0

                logging.info(u'  ' + out)
                # longitude = 131
                # latitude = 43
                if latitude_gps>0 and longitude_gps>0:
                    post_position_test()

        # ret = urllib2.urlopen('http://95.154.96.240:8000/save/?longitude=131.1&latitude=43.1')

        #

        # print (ret)

        time.sleep(5)




        # except KeyboardInterrupt:
        #    del robo
