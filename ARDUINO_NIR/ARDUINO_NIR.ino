#include <Servo.h>
#include "QList.h"
#include "QList.cpp"
#include "arduino_nir.h"
#include "TinyGPS.h"
#include <SoftwareSerial.h>
#include <LiquidCrystal.h>

Servo myServo;
//Servo myMotor;
Servo myMotorL;
Servo myMotorR;

//SoftwareSerial dbgTerminal(PIN_STOROG_RX, PIN_STOROG_TX); // RX, TX
LiquidCrystal lcd(4, 5, 6, 7, 8, 9);

TinyGPS gps;

//SoftwareSerial mySerial = SoftwareSerial(3, 2);



struct gps_point
{
  float lat;
  float lon;
};

QList<String> myList;

QList<gps_point> gpsPoints;
int count_gps_count = -1;

//String str_list[100];
int count_str = 0;

double start = millis();
unsigned long timer = 0;
unsigned long timer_simulator = millis() + 1000;
bool m = false;
bool start_connect = false;
String inputStr = "";
unsigned long timer_storog = 0;
int count_tmp = 0;
double angle = 0; // переменная угл с компаса {-180;180}
double channel_1 = 1500;
double channel_2 = 1500;
double channel_3 = 1500;
double azimut = -1;
int flag_internet = 0;
double vcc = 0;

double kp = 4;
double start_speed_motor = 300; // 1530 минимальный газ для спартана
double speed_motor = 300; // 1530 минимальный газ для спартана
double serva_angle = 0; //
double sim_angle = 180; //



// наши координаты
float lat_gps = 0; //43.130210;
float long_gps = 0;// 131.889236;

float lat_home = 0; //  43.142519
float long_home = 0; //131.907363;

float lat_point =  43.141723; //43.078834;
float long_point = 131.906997; //131.928481;

double lat_small_point =  0; //43.141723;//43.078834;
double long_small_point = 0; //131.906997;//131.928481;

double lat_sim =  43.142519;
double long_sim = 131.907363;

unsigned short satellites_gps = 0;
unsigned long course_gps = 0;
float speed_gps = 0;
long altitude_gps = 0;
unsigned long hdop_gps = 0;
int min_dist = 1000;

unsigned long timer_working = millis();

void setup() {
  communication_setup();
  beep5_fast();
  rc_setup();
  setup_vcc();
  lcd_setup();
  

  low_level_setup();
  storog_setup();
  setup_compas();
  gps_setup();

  delay(10000);
  beep5_fast();

  //Serial.println("Setup OK" );
}
void time_work(String s)
{
  //Serial.print( " " + s + " "+ String(millis() - timer_working));
  timer_working =  millis();
}
void loop() {


  modem_setup2();
  time_work(" modem setup");
  modem_work2();
  time_work(" modem work");
  vcc_work();

  comm_work();
  //Serial.print("comm_work, " );
  rc_work();
  //Serial.print("rc_work, " );
  storog_work();
  //Serial.print("storog_work, " );
  compas_work();
  //sim_compas_work();
  //Serial.print("compas_work, " );
  gps_work();
  //sim_gps_work();
  //Serial.println("gps_work, " );
  time_work(" all work\n");





  /// ручной режим
  if (channel_3 < 1300 && channel_3 > 900)
  {

    //rc_manual();  // 1 мотор и руль
    rc_manual_2_motors();  // 2 мотора

  }

  if (channel_3 > 1300 && channel_3 < 1600)
  {
    move_azimut();
  }

  if ((channel_3 > 1800 && channel_3 < 2100) || channel_3 < 100 )
  {
    move_gps(lat_small_point, long_small_point);
    
    
    azimut = -1;
  }

  // при выключеном пульте лодка начинает идти по заданным точкам
  if (channel_3 < 900)
  {
    move_gps(lat_small_point, long_small_point);
    //Serial.println("no radio" );
    //   Serial.println(channel_3);
  }



  lcd.setCursor(11, 0);
  lcd.print(flag_internet);
  lcd.setCursor(13, 0);
  lcd.print(angle);




  // float c1 = gps.course_to (lat_home, long_home, lat_point, long_point);
  //  Serial.println(c1);
  //  delay(1000);



  /*
    // тест жпс функций
      double d =  distanceLL( 43+3/60+59.89/3600, 131+54/60+45.39/3600, 43+3/60+36.40/3600, 131+54/60+9.17/3600);
      long d2 =  CalcDistance( 43+3/60+48.76/3600, 131+54/60+41.98/3600, 43+3/60+36.40/3600, 131+54/60+9.17/3600);


      Serial.println(d);
      Serial.println(d2);

      double a = CalcBearing(43+3/60+59.89/3600, 131+54/60+45.39/3600, 43+3/60+36.40/3600, 131+54/60+9.17/3600);
      double a2 = CalcBearing(43+4/60+20.27/3600, 131+55/60+19.12/3600, 43+3/60+59.89/3600, 131+54/60+45.39/3600);
      Serial.println(a);
      Serial.println(a2);

      double l1=0;
      double l2=0;
      ComputeDestPoint( 43+3/60+36.40/3600, 131+54/60+9.17/3600, 25, 100, &l1, &l2);
      Serial.println(String(l1));
      Serial.println(String(l2));

      point();
  */

  //Serial.println(String(angle) +" "+String( sin((180 -angle)/(180/PI)) *(180/PI) ) );
  //serva_move(angle);
  //while (true);

  /*
         /// для теста сторожа
      if(count_tmp>20)
        {
          storog_reset_all();
          count_tmp=0;

        }
  */

}



