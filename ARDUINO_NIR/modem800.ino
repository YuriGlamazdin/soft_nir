/*************************************************************************
  Test sketch for SIM800 library
  Distributed under GPL v2.0
  Written by Stanley Huang <stanleyhuangyc@gmail.com>
  For more information, please visit http://arduinodev.com
*************************************************************************/

#include "SIM800.h"

#define APN "internet"
#define con Serial
//static const char* url = "http://arduinodev.com/datetime.php";
//static const char* url = "http://95.154.96.240:8000/save/?longitude=131.91809&latitude=43.087212";
//static const char* url = "http://95.154.96.240:8000/save/";


CGPRS_SIM800 gprs;
uint32_t count = 0;
uint32_t errors = 0;

unsigned long modem_setup_timer = 0;
int modem_setup_count = 0;
int modem_setup_pause = 0;
bool modem_setup_flag = true;
bool flag_end = false;
int tmp = 0;
unsigned long modem_work_timer = 0;
int modem_work_count = 0;
int modem_work_pause = 0;
bool modem_work_flag = false;
char buffw[1024]; // Массив для вывода
String html;
int count_reset=0;
String s;
char frame[255];


void modem_setup_timer_set(unsigned int t)
{
  // через сколько времени сработает след пункт
  modem_setup_pause = t;
  modem_setup_timer = millis();
  modem_setup_count++;

}
void modem_work_timer_set(unsigned int t)
{
  // через сколько времени сработает след пункт
  modem_work_pause = t;
  modem_work_timer = millis();
  modem_work_count++;

}
void modem_setup2()
{
  if (!modem_setup_flag) return;

  if (modem_setup_timer + modem_setup_pause < millis())
  {
    tmp = 0;
    switch (modem_setup_count) {

      case 0:
        
        modem_work_flag = false;
        //con.print("setup start");
        modem_setup_timer_set(5000);
        break;
      case 1:
        count_reset=0;
        SIM_SERIAL.begin(115200);
        pinMode(SIM800_RESET_PIN, OUTPUT);
        digitalWrite(SIM800_RESET_PIN, HIGH);
        delay(10);
        digitalWrite(SIM800_RESET_PIN, LOW);
        delay(100);
        digitalWrite(SIM800_RESET_PIN, HIGH);
        modem_setup_timer_set(1000);
        break;

      case 2:
        //delay(1000);
        tmp = gprs.sendCommand("AT", 200);


        modem_setup_timer_set(2000);
        if (tmp == 0)  modem_setup_count = 2;
        count_reset++;
        if(count_reset>20) modem_setup_count = 0;
        
        break;

      case 3:
        //delay(1000);
        gprs.sendCommand("AT+IPR=115200", 500);
        modem_setup_timer_set(1000);
        break;

      case 4:
        //delay(1000);
        gprs.sendCommand("ATE0", 500);
        modem_setup_timer_set(1000);
        break;

      case 5:
        //delay(1000);
        gprs.sendCommand("AT+CFUN=1", 200);
        modem_setup_timer_set(1000);
        break;

      case 6:

        gprs.sendCommand("AT+CBAND=DCS_MODE", 200);  /// мегафон
        //sendCommand("AT+CBAND=ALL_BAND", 2000);

        //sendCommand("AT+CBAND?", 2000);
        modem_setup_timer_set(3000);
        count_reset=0;
        break;
      case 7:
        flag_end = false;

        if (gprs.sendCommand("AT+CREG?", 500)) {
          //delay(1000);
          char *p = strstr(gprs.buffer_m, "0,");
          if (p) {
            char mode = *(p + 2);
            if (mode == '1' || mode == '5') {
              flag_end = true;

            }
          }

        }
        if (!flag_end)
        {
          modem_setup_pause = 2000;
          modem_setup_timer = millis();
          modem_setup_count = 7;
          count_reset++;
          if(count_reset>30) modem_setup_count=0;
        }
        else
        {
          modem_setup_timer_set(3000);
        }



        break;
      case 8:
        //delay(1000);
        gprs.sendCommand("AT+SAPBR=3,1,\"CONTYPE\",\"GPRS\"", 500);
        modem_setup_timer_set(1000);
        break;
      case 9:
        //delay(1000);
        //gprs.sendCommand("AT+SAPBR=3,1,\"APN\",\"internet\"", 10000);
        gprs.sendCommand("AT+SAPBR=3,1,\"APN\",\"internet.mts.ru\"", 500);
        modem_setup_timer_set(1000);
        break;
      case 10:
        //delay(1000);
        gprs.sendCommand("AT+SAPBR=3,1,\"USER\",\"gdata\"", 500);
        modem_setup_timer_set(1000);
        break;
      case 11:
        gprs.sendCommand("AT+SAPBR=3,1,\"PWD\",\"gdata\"", 500);
        modem_setup_timer_set(1000);
        count_reset=0;
        break;

      case 12:
        gprs.sendCommand("AT+SAPBR=1,1", 200);
        modem_setup_timer_set(3000);
        break;

      case 13:
        //con.println("Set GPRS");
        flag_end = false;
        if (gprs.sendCommand("AT+SAPBR=2,1", 200)) {
          //delay(1000);
          //Serial.print("buffer:");
          //Serial.println(buffer_m);
          char *p = strstr(gprs.buffer_m, "1,");
          if (p) {
            char mode = *(p + 2);
            //#if DEBUG
            //Serial.print("GPRS Mode:");
            //Serial.println(mode);
            //#endif
            if (mode == '1') {
              flag_end = true;
            }
          }
        }

        if (!flag_end)
        {
          modem_setup_pause = 3000;
          modem_setup_timer = millis();
          modem_setup_count = 12;
          count_reset++;
          if(count_reset>20)modem_setup_count=0;
        }
        else
        {
          modem_setup_timer_set(3000);
        }




        break;
        
        case 14:        
        gprs.sendCommand("AT + HTTPINIT", 200);
        beep1_long();
        modem_setup_timer_set(1000);
        break;


      default:
        //Serial.println("END SETUP");
        modem_setup_flag = false;
        modem_work_flag = true;
        modem_setup_count = 0;

        break;
    }
  }
}

void modem_work2()
{
  if (!modem_work_flag) return;

  if (modem_work_timer + modem_work_pause < millis())
  {
    tmp = 0;
    switch (modem_work_count) {

      case 0:
        /// Если нет интернета просто ресетим
        //con.println("Set GPRS");
        flag_end = false;
        if (gprs.sendCommand("AT+SAPBR=2,1", 200)) {
          //delay(1000);
          //Serial.print("buffer:");
          //Serial.println(buffer_m);
          char *p = strstr(gprs.buffer_m, "1,");
          if (p) {
            char mode = *(p + 2);
            //#if DEBUG
            //Serial.print("GPRS Mode:");
            //Serial.println(mode);
            //#endif
            if (mode == '1') {
              flag_end = true;
            }
          }
        }

        if (!flag_end)
        {
          modem_setup_flag = true;
          modem_work_flag = false;
          modem_work_count = 0;
          break;
        }
        else
        {
          modem_work_timer_set(3000);
        }
        break;

      case 1:

        modem_work_timer_set(100);
        break;

      case 2:

        gprs.sendCommand("AT+HTTPPARA=\"CID\",1", 200);
        modem_work_timer_set(2000);
        break;

      case 3:
        
        if(long_gps==0 || lat_gps==0) 
        {                    
          modem_work_pause = 3000;
          modem_work_timer = millis();
          modem_work_count = 0;
        //  Serial.println("no gps");
          
          break;
        }

        gprs.sendCommand("AT+HTTPPARA=\"URL\",\"http://95.154.96.240:8000/api/position/post/\"", 100);
        //gprs.sendCommand("AT+HTTPPARA=\"URL\",\"http://85.95.150.144:666/api/position/post/\"", 100);
        //
        modem_work_timer_set(2000);
        
        break;
      
      case 4:
        
        s="{\"data\": {\"latitude_gps\": \""+String(lat_gps,7)+"\", \"longitude_gps\": \""+String(long_gps,7)+"\", \"vcc\": \""+String(vcc)+"\", \"yaw_compass\": \""+String(angle)+"\", \"altitude_gps\": \""+String(altitude_gps)+"\", \"yaw_gps\": \""+String(course_gps)+"\", \"speed_gps\": \""+String(speed_gps)+"\", \"satellites_gps\": \""+String(satellites_gps)+"\", \"hdop_gps\": \""+String(hdop_gps)+"\", \"latitude_point\": \""+String(lat_small_point,7)+"\", \"longitude_point\": \""+String(long_small_point,7)+"\", \"image\": \"0\"}, \"key\": \"around\"}\r\n";
        gprs.sendCommand("AT+HTTPDATA="+String(s.length())+",1500", 100);
        modem_work_timer_set(500);
        break;
      case 5:


//              "altitude_gps": str(altitude_gps),  #
//             "yaw_gps": str(yaw_gps),  # -
//             "speed_gps": str(speed_gps),  # -
//             "satellites_gps": str(satellites_gps),  # -
//             "hdop_gps": str(hdop_gps),  #  -
//             "latitude_point": str(latitude_point),  #
//             "longitude_point": str(longitude_point),

             
        gprs.sendCommand(s, 100);
        modem_work_timer_set(500);
        break;

      case 6:
        gprs.sendCommand("AT+HTTPPARA=\"CONTENT\",\"application/json\"", 100);
        modem_work_timer_set(500);
        break;


      case 7:
        gprs.sendCommand("AT+HTTPACTION=1", 100);
        //gprs.sendCommand("AT+HTTPREAD=1000,1000", 1000);
        modem_work_timer_set(500);
        break;

      case 8:
        //gprs.sendCommand("AT+HTTPREAD=1000,1000", 100);
        //gprs.sendCommand("AT+HTTPACTION=1", 100);
        gprs.sendCommand(0,100);

        modem_work_timer_set(1000);
        break;
      case 9:
        //gprs.sendCommand("AT+HTTPREAD", 1000);
        //gprs.sendCommand(0, 100);
        //html = gprs.buffer_m;
        //Serial.println(html);
        modem_work_timer_set(100);
        break;

      default:
        modem_work_timer_set(100);
        modem_work_count = 0;

        break;
    }
  }
}

String ReadGSM() {  //функция чтения данных от GSM модуля
  int c;
  String v;
  while (Serial2.available()) {  //сохраняем входную строку в переменную v
    c = Serial2.read();
    v += char(c);
    delay(10);
  }
  return v;
}

