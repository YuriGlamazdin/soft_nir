

#define RC_PIN_1 A0
#define RC_PIN_2 A1
#define RC_PIN_3 A2

//#define MOTOR_PIN 10
//#define SERVA_PIN 11

#define MOTOR_L_PIN 10
#define MOTOR_R_PIN 11
#define SERVA_PIN_FLUGER 47

#define PIN_STOROG_RX   3
#define PIN_STOROG_TX   2

#define PIN_BEEPER   51

#define PIN_VCC A5

#define PAUSE_STOROG   3000




void low_level_setup();
void communication_setup();
void comm_work();
void storog_setup();
void storog_work();
void storog_reset_all();
double distanceLL(double , double , double , double );
double angleFromCoordinate(double , double , double , double );
long CalcDistance(double , double , double , double );
int CalcBearing(double , double , double , double );
//void ComputeDestPoint(long lat1, long lon1, double iBear, double iDist, long *lat2, long *lon2);
void point();
void setup_compas();
void compas_work();
void serva_move(int angle);
void rc_setup();
void rc_work();
//void motor_move(int speed_motor);
void rc_manual();
void move_azimut(int a);
void gps_setup();
void gps_work();
void lcd_setup();
void modem_setup();
void modem_work();

