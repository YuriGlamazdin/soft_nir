void sim_compas_work()
{
  sim_angle = sim_angle + serva_angle / 10 + (1 - random(2));
  //sim_angle-=1;
  if (sim_angle > 360)
    sim_angle = sim_angle - 360;
  if (sim_angle < 0)
    sim_angle = 360 + sim_angle;

  /// делаем магнитное отклонение 
  double angle_mag_sim = sim_angle-0;
  if (angle_mag_sim > 360)
    angle_mag_sim = angle_mag_sim - 360;
  if (angle_mag_sim < 0)
    angle_mag_sim = 360 + angle_mag_sim;
  
  angle = angle_mag_sim;

  //Serial.println("sim_angle: "+ String(sim_angle)+ " serva_angle " +String(serva_angle));
  //Serial.println(" "+String(angle)+ " ");


}



void sim_gps_work()
{


  if (timer_simulator > millis()) return;
  timer_simulator = millis() + 5000;//10000;


  double lat = 0;
  double lon = 0;


  float jump = 9;
  if (speed_motor < 1520)
    jump = 0;

   // angle=92;
    
  ComputeDestPoint( lat_sim, long_sim, angle, jump, &lat, &lon);
//  ComputeDestPoint( lat_sim, long_sim, angle, 1000, &lat, &lon);
//  double revert_angle = angle+180;
//    if (revert_angle > 360)
//    revert_angle = revert_angle - 360;
//      
  lat_gps = lat;//+random(50)*0.000001;
  long_gps = lon;//+random(50)*0.000001;;
  lat_sim = lat;
  long_sim  = lon;
  double veter=140+50 - random(100);  // 
  int sila_vetra = random(12);
  ComputeDestPoint( lat_sim, long_sim, veter, sila_vetra, &lat, &lon);
  
  //CoordinateToCoordinate (lat_sim, long_sim, 20, jump, &lat, &lon);

  lat_gps = lat;//+random(50)*0.000001;
  long_gps = lon;//+random(50)*0.000001;;
  lat_sim = lat;
  long_sim  = lon;

  satellites_gps = 12;
  long dist =  CalcDistance( lat_point, long_point, lat_gps, long_gps);
  long dist_small =  CalcDistance( lat_small_point, long_small_point, lat_gps, long_gps);

//  Serial.print(" "+String(lat_gps*1000000) + "\t"+String(long_gps*1000000)+ "\t");
  //Serial.print(String(lat_point * 1000000) + "\t" + String(long_point * 1000000));
  //Serial.println(" " +String(count_gps_count) +" (" + String(gpsPoints.size())+") dist_point" + String(dist) + " dist_small="+ String(dist_small) + " angle = " + String(angle) + " kp=" + String(kp) + " speed_motor=" + String(speed_motor));

  /*

    gps.f_get_position(&lat_gps, &long_gps, &age);

    satellites_gps = gps.satellites();
    course_gps = gps.course();
    speed_gps = gps.speed();
    hdop_gps = gps.hdop();
    lcd.clear();
    //lcd.setCursor(0, 1);
    //lcd.print(lat_gps == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : lat_gps, 6);
    lcd.setCursor(0, 0);
    lcd.print(long_gps == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : long_gps, 6);
    lcd.setCursor(11, 1);
    lcd.print(satellites_gps);
    lcd.setCursor(13, 1);
    lcd.print(course_gps);
    lcd.setCursor(0, 1);
    lcd.print(speed_gps);
    lcd.setCursor(3, 1);
    lcd.print(hdop_gps);
  */

  gps_point_next();
  save_home();



}
