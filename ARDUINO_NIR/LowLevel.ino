void low_level_setup()
{

  //pinMode(MOTOR_PIN, OUTPUT);

  /*
    /// 1 мотор 1 руль
    myServo.attach(SERVA_PIN);
    myServo.write(90); // открыли захват
    myMotor.attach(MOTOR_PIN);
  */
  myServo.attach(SERVA_PIN_FLUGER);

  myMotorL.attach(MOTOR_L_PIN);
  myMotorR.attach(MOTOR_R_PIN);


}

double LineAngle(double x1, double y1, double x2, double y2)
{
  double angle = atan2(y2 - y1, x2 - x1) * 180.0 / PI;
  return angle;
}

double StopMotor()
{

  motor_moveL(1500);
  motor_moveR(1500);
}

double error_kurs(double azimut1, double angle1, double deviation )
{

  /// добавление магнитной поправки в компас.
  double mag_error = deviation; //20
  angle1 = angle1 + mag_error;
  if ( angle1 > 360)
    angle1 =  angle1 - 360;
  if ( angle1 > 360)
    angle1 =  angle1 - 360;
  if (angle1 < 0)
    angle1 = 360 + angle1;



  double e =  azimut1 - angle1 ;

  if (abs(e) > 180)
  {
    if (azimut1 < angle1)
    {
      e = azimut1 + 360 - angle1;
      // Serial.print("1- " );
    }
    else
    {
      e = azimut1 - 360 - angle1 ;
      // Serial.print("2- " );
    }
  }
  return e;
}



void change_param()
{
  if (channel_3 < 1300 && channel_3 > 2000) return;

  if (channel_1 > 1700 && channel_1 < 2010)
  {


    beep1_fast();
    delay(1000);
    if (channel_3 > 1300 && channel_3 < 2000)
      speed_motor += 50;

    if (speed_motor > 500)
      speed_motor = 500;
  }
  if (channel_1 > 950 && channel_1 < 1300  )
  {


    beep1_fast();
    delay(1000);

    if (channel_3 > 1300 && channel_3 < 2000)
      speed_motor -= 50;

    if (speed_motor < 0)
      speed_motor = 0;
  }


  //    motor_move(channel_1);
  //  else
  //    motor_move(1535);
  //StopMotor();

  if (channel_2 > 1600 && channel_2 < 2010)
    kp += 0.05;

  if (channel_2 < 1400 && channel_2 > 1000)
    kp -= 0.05;

  if (kp < 0)
    kp = 0;

  //Serial.println(String(speed_motor) + " " + String(kp));
}

void move_gps(double lat_p, double long_p)
{
  if (long_home == 0 && lat_home == 0)
  {
    // пока дом не зафиксирован. никуда не плывем
    StopMotor();
    return;
  }


  azimut = -1;
  // движемся с текущих координат в точку назначения

  /// сначала вычисляем угол на точку
  double a1 = gps.course_to (lat_gps, long_gps, lat_p, long_p);
  // вычисляем ошибку от текущего направления по компасу
  /// поправка с отклонением компаса
  double e = error_kurs(a1, angle, -10.3 );


  //kp=1;
  double p = e * kp;

  change_param();

  serva_move(p * 2);
  //motor_move(speed_motor);

  //Serial.println(String(a1)+" angle="+String(angle)+" e="+String(e)+" p="+String(p) );


  motor_moveL(1500 + speed_motor - p);
  motor_moveR(1500 + speed_motor + p);

}

void move_azimut()
{

  /// при включении режима, запоминаем текущее направление и движемся по нему
  if (azimut == -1)
  {
    azimut = angle;
    speed_motor = start_speed_motor;
  }

  double e = error_kurs(azimut , angle, 0 );
  //  Serial.print("!-");
  //  Serial.print(String(azimut) +" | ");
  //  Serial.print(String(angle) +" | ");
  //  Serial.println(String(err));

  //kp=15;
  double p = e * kp;
  //Serial.println("azimut " + String(azimut) + " angle " + String(angle) + " p=" + String(e));
  change_param();

  // 1 мотор 1 руль
  serva_move(p);
  //motor_move(speed_motor);

  //motor_move(1010);



  motor_moveL(1500 + speed_motor - p);
  motor_moveR(1500 + speed_motor + p);




}

void serva_move(int angle)
{
  serva_angle = constrain(angle, -90, 90);
  if (serva_angle < 0)
    myServo.write(90 + serva_angle * -1);
  if (serva_angle >= 0)
    myServo.write(90 - serva_angle);

  //Serial.println("Serva move " + String(angle) );
}

/*
  void motor_move(int speedm)
  {
  //Serial.println("Move " + String(speed_motor) );
  myMotor.writeMicroseconds(speedm);


  }
*/
void motor_moveL(int speed_motor)
{
  //Serial.println("MoveL " + String(speed_motor) );
  speed_motor = constrain(speed_motor, 1000, 2000);

  myMotorL.writeMicroseconds(speed_motor);


}
void motor_moveR(int speed_motor)
{
  //Serial.println("MoveR " + String(speed_motor) );
  speed_motor = constrain(speed_motor, 1000, 2000);
  myMotorR.writeMicroseconds(speed_motor);


}



