//#define PI 3.14159265358979323846
#define earthRadiusKm 6371000.0

//static const double PI = 3.14159265358979323846,
#define earthDiameterMeters  6371.0 * 2 * 1000 //

void gps_setup()
{
  lcd.begin(16, 2);
  lcd.print("gps wait");

  Serial1.begin(9600);
  //robocenter_points();
  ozero_chan_points();
  //pechchany_points();
  //to_japan_points();

}


void gps_point_next()
{
  //return;
  /// если начали работу, назначаем первую точку
  //Serial.print(String(count_gps_count) + " ");
  if (count_gps_count == -1)
    if (gpsPoints.size() > 0)
    {
      count_gps_count = 0;
      gps_point p = gpsPoints.at(count_gps_count);
      lat_point =  p.lat;
      long_point =  p.lon;

      lat_small_point = lat_gps;
      long_small_point = long_gps;
    }

  // плывем к локальной точке
  long dist_loc =  CalcDistance( lat_small_point, long_small_point, lat_gps, long_gps);

  if (dist_loc < 5  `)
  {
    beep2_fast();
    //Serial.println(" make small point");
    // делаем новую локальную точку, либо плывем к глобальной
    long dist_to_point =  CalcDistance( lat_small_point, long_small_point, lat_point , long_point);
    if (dist_to_point > min_dist)
    {
      // делаем очередную локальную точку
      double course1 = gps.course_to (lat_small_point, long_small_point, lat_point , long_point);
     // Serial.println(" course " + String(course1));

      ComputeDestPoint( lat_small_point, long_small_point, course1, min_dist, &lat_small_point, &long_small_point);
      

    }
    else
    {
      // если дистанция меньше минимальной просто плывем на точку
      lat_small_point = lat_point;
      long_small_point = long_point;
    }

  }
  
  long dist =  CalcDistance( lat_point, long_point, lat_gps, long_gps);

  if (dist < 5)
  {
    if (count_gps_count == gpsPoints.size())
    {
      StopMotor();
      speed_motor = 0;
      //beep1();
      //delay(10000);
      return;
    }

    count_gps_count++;
    if (count_gps_count < gpsPoints.size())
    {
      gps_point p = gpsPoints.at(count_gps_count);
      lat_point =  p.lat;
      long_point =  p.lon;
    }
  }

}


void save_home()
{
  // запоминаеем точку старта
  /// пока при старте новая. дальше надо сохранять в память и по команде обновлять
  if (long_home == 0 && lat_home==0)
    if (satellites_gps > 5 && satellites_gps <255)
    {

      long_home = long_gps;
      lat_home = lat_gps;

      // добавляем последнюю точку дом
      gps_point p_home;
      p_home.lat = lat_home;
      p_home.lon = long_home;
      gpsPoints.push_back(p_home);

      speed_motor = start_speed_motor;

      beep3();
    }
}
void add_points(int min_dist)
{
  QList<gps_point> gpsPoints_new;

  // добавляем текущие координаты.
  gps_point p_now;
  p_now.lat = lat_gps;
  p_now.lon = long_gps;
  gpsPoints.push_front(p_now);

  for (int i = 0; i < gpsPoints.size(); i++)
  {
    gps_point p1 = gpsPoints.at(i);
    gpsPoints_new.push_back(p1);
    if (i + 1 < gpsPoints.size())
    {
      gps_point p2 = gpsPoints.at(i + 1);




      int dist =  CalcDistance( p1.lat, p1.lon, p2.lat, p2.lon) / min_dist;
      //Serial.println(" dist " + String(dist));
      double course1 = gps.course_to (p1.lat, p1.lon, p2.lat, p2.lon);
      //Serial.println(" course " + String(course1));

      for (int j = 0; j < dist; j++)
      {

        double lat3;
        double lon3;
        ComputeDestPoint( p1.lat, p1.lon, course1, min_dist + min_dist * j, &lat3, &lon3);
        gps_point pp;
        pp.lat = lat3;
        pp.lon = lon3;
        gpsPoints_new.push_back(pp);
        //Serial.println(String(j) + " " + String(lat3 * 100000));
      }

    }
  }
  //Serial.println("clear");
  gpsPoints.clear();
  if (gpsPoints_new.size() > 0)
    for (int i = 1; i < gpsPoints_new.size(); i++)
    {
      //Serial.println("add " + String(i));
      gpsPoints.push_back(gpsPoints_new.at(i));
    }

  //Serial.println("all_points");

  for (int i = 0; i < gpsPoints.size(); i++)
  {
    gps_point p1 = gpsPoints.at(i);
    //Serial.println("lat " + String(p1.lat * 1000000) + " lon " + String(p1.lon * 1000000));
  }
  count_gps_count  = -1;

  //Serial.println("Eng points work");

}

double degreeToRadian (const double degree) {
  return (degree * PI / 180);
};
double radianToDegree (const double radian) {
  return (radian * 180 / PI);
};


void robocenter_points()
{

  lat_sim =  43.142519;
  long_sim = 131.907363;

//  gps_point p1;
//  p1.lat = 43.141723;
//  p1.lon = 131.906997;
//  gpsPoints.push_back(p1);
//
//  gps_point p2;
//  p2.lat = 43.141631;
//  p2.lon = 131.906479;
//  gpsPoints.push_back(p2);
//
//  gps_point p3;
//  p3.lat = 43.142083;
//  p3.lon = 131.906239;
//  gpsPoints.push_back(p3);

  gps_point p4;
  p4.lat =   43.130369;
  p4.lon = 131.888723;
  gpsPoints.push_back(p4);

//  gps_point p5;
//  p5.lat = 43.141723;
//  p5.lon = 131.906997;
//  gpsPoints.push_back(p5);
}
void pechchany_points()
{

  lat_sim =   43.133670;
  long_sim = 131.884074;


  gps_point p1;
  p1.lat =  43.19967;
  p1.lon = 131.775612;
  gpsPoints.push_back(p1);

  gps_point p2;
  p2.lat =  43.174496;
  p2.lon = 131.782951;
  gpsPoints.push_back(p2);

  gps_point p3;
  p3.lat =  43.152547;
  p3.lon = 131.740727;
  gpsPoints.push_back(p3);

  //  gps_point p4;
  //  p4.lat = 43.142182;
  //  p4.lon = 131.906799;
  //  gpsPoints.push_back(p4);
}

void ozero_chan_points()
{

  lat_sim =   43.142461;
  long_sim = 131.907452;

 
//  gps_point p10;
//  p10.lat = 43.142028;
//  p10.lon = 131.907293;
//  gpsPoints.push_back(p10);


  gps_point p1;
  p1.lat =  43.142086;
  p1.lon = 131.907324;
  gpsPoints.push_back(p1);

  gps_point p2;
  p2.lat =  43.141634;
  p2.lon = 131.907116;
  gpsPoints.push_back(p2);

  gps_point p3;
  p3.lat =  43.141808;
  p3.lon = 131.906466;
  gpsPoints.push_back(p3);

  gps_point p4;
  p4.lat =  43.142253;
  p4.lon = 131.906659;
  gpsPoints.push_back(p4);

  gps_point p5;
  p5.lat = 43.142086;
  p5.lon = 131.907324;
  gpsPoints.push_back(p5);

}

void to_japan_points()
{

  lat_sim =   43.133670;
  long_sim = 131.884074;

  int m_size = 8;
  double points[m_size][2] = {
    { 43.117136, 131.848754},
    { 43.072803, 131.834182},
    { 43.062594, 131.906595},  // мост
    
    { 42.785894, 132.354955},
    { 42.695643, 132.374052},
    { 43.062594, 131.906595},  // мост
    { 43.072803, 131.834182},
    { 43.117136, 131.848754}

  };

  for (int i = 0; i < m_size; i++)
  {
    gps_point p1;

    p1.lat = points[i][0];
    p1.lon = points[i][1];
   // Serial.println(String(p1.lat * 1000000) + " " + String(p1.lon * 1000000));
    gpsPoints.push_back(p1);
  }
}



void gps_work()
{
  bool newData = false;
  unsigned long chars;
  unsigned short sentences, failed;

  // For one second we parse GPS data and report some key values
  //for (unsigned long start = millis(); millis() - start < 1000;)
  //{

  while (Serial1.available())
  {

    //char c = mySerial.read();
    char c = Serial1.read();
    // Serial.write(c); // uncomment this line if you want to see the GPS data flowing
    if (gps.encode(c)) // Did a new valid sentence come in?
      newData = true;

  }
  //}

  if (newData)
  {
    unsigned long age;
    gps.f_get_position(&lat_gps, &long_gps, &age);

    satellites_gps = gps.satellites();
    course_gps = gps.course();
    //speed_gps = gps.speed();
    speed_gps = gps.f_speed_kmph();
    
    hdop_gps = gps.hdop();
    lcd.clear();
    //lcd.setCursor(0, 1);
    //lcd.print(lat_gps == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : lat_gps, 6);
    lcd.setCursor(0, 0);
    lcd.print(long_gps == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : long_gps, 6);
    lcd.setCursor(11, 1);
    lcd.print(satellites_gps);
    lcd.setCursor(13, 1);
    lcd.print(course_gps);
    lcd.setCursor(0, 1);
    lcd.print(speed_gps);
    lcd.setCursor(3, 1);
    lcd.print(hdop_gps);


    gps_point_next();
    save_home();
  }


}


// This function converts decimal degrees to radians
double deg2rad(double deg) {
  return (deg * PI / 180);
}

//  This function converts radians to decimal degrees
double rad2deg(double rad) {
  return (rad * 180 / PI);
}
//convert degrees to radians
double dtor(double fdegrees)
{
  return (fdegrees * PI / 180);
}

//Convert radians to degrees
double rtod(double fradians)
{
  return (fradians * 180.0 / PI);
}
/**
  Returns the distance between two points on the Earth.
  Direct translation from http://en.wikipedia.org/wiki/Haversine_formula
  @param lat1d Latitude of the first point in degrees
  @param lon1d Longitude of the first point in degrees
  @param lat2d Latitude of the second point in degrees
  @param lon2d Longitude of the second point in degrees
  @return The distance between the two points in kilometers
*/
double distanceLL(double lat1d, double lon1d, double lat2d, double lon2d) {
  double lat1r, lon1r, lat2r, lon2r, u, v;
  lat1r = deg2rad(lat1d);
  lon1r = deg2rad(lon1d);
  lat2r = deg2rad(lat2d);
  lon2r = deg2rad(lon2d);
  u = sin((lat2r - lat1r) / 2);
  v = sin((lon2r - lon1r) / 2);
  return 2.0 * earthRadiusKm * asin(sqrt(u * u + cos(lat1r) * cos(lat2r) * v * v));
}


//Calculate distance form lat1/lon1 to lat2/lon2 using haversine formula
//Note lat1/lon1/lat2/lon2 must be in radians
//Returns distance in feet
long CalcDistance(double lat1, double lon1, double lat2, double lon2)
{
  double dlon, dlat, a, c;
  double dist = 0.0;
  dlon = dtor(lon2 - lon1);
  dlat = dtor(lat2 - lat1);
  a = pow(sin(dlat / 2), 2) + cos(dtor(lat1)) * cos(dtor(lat2)) * pow(sin(dlon / 2), 2);
  c = 2 * atan2(sqrt(a), sqrt(1 - a));

  dist = 6378140 * c;  //radius of the earth (6378140 meters) in feet 20925656.2
  return ( (long) dist + 0.5);
}


//Calculate bearing from lat1/lon1 to lat2/lon2
//Note lat1/lon1/lat2/lon2 must be in radians
//Returns bearing in degrees
int CalcBearing(double lat1, double lon1, double lat2, double lon2)
{
  lat1 = dtor(lat1);
  lon1 = dtor(lon1);
  lat2 = dtor(lat2);
  lon2 = dtor(lon2);

  //determine angle
  double bearing = atan2(sin(lon2 - lon1) * cos(lat2), (cos(lat1) * sin(lat2)) - (sin(lat1) * cos(lat2) * cos(lon2 - lon1)));
  //convert to degrees
  bearing = rtod(bearing);
  //use mod to turn -90 = 270
  bearing = fmod((bearing + 360.0), 360);
  return (int) bearing + 0.5;
}

void CoordinateToCoordinate (float latitude, float longitude, double angle, double meters, float * lat2, float * lon2)
{
  latitude = degreeToRadian(latitude);
  longitude = degreeToRadian(longitude);
  angle = degreeToRadian(angle);
  meters *= 2 / earthDiameterMeters;




  *lat2 = asin((sin(latitude) * cos(meters))
               + (cos(latitude) * sin(meters) * cos(angle)));
  *lon2 = longitude + atan2((sin(angle) * sin(meters) * cos(latitude)),
                            cos(meters) - (sin(latitude) * sin(*lat2)));

  *lat2 = radianToDegree(*lat2);
  *lon2 = radianToDegree(*lon2);

}


void ComputeDestPoint(double lat1, double lon1, double iBear, double iDist, double * lat2, double * lon2)
{
  double bearing = dtor((double) iBear);
  double dist = iDist / 6378140.0;


  lat1 = dtor(lat1);
  lon1 = dtor(lon1);
  *lat2 = asin(sin(lat1) * cos(dist) + cos(lat1) * sin(dist) * cos(bearing));
  *lon2 = lon1 + atan2(sin(bearing) * sin(dist) * cos(lat1), cos(dist) - sin(lat1) * sin(*lat2));
  //*lon2 = fmod( *lon2 + 3 * PI, 2 * PI ) - PI;
  *lon2 = rtod( *lon2);
  *lat2 = rtod( *lat2);
}



